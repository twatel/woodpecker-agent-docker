##
## —————————————— MAKEFILE ————————————————————————————————————————————————————————
##
SHELL=/bin/sh

.DEFAULT_GOAL = help

.PHONY: help
help: ## Display help
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/Makefile://' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-22s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


.PHONY: header
header:
	@echo "*********************************** MAKEFILE ***********************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE 	`uname -r`"
	@echo "KERNEL VERSION 	`uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

##
## —————————————— ENVIRONMENTS CONFIGURATION ——————————————————————————————————————
##
.PHONY: env
env: header ## Prepare environment
	@[ -d "${PWD}/.direnv" ] || (echo "Venv not found: ${PWD}/.direnv" && exit 1)
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} pip3" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} pip3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} wheel" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} wheel"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} setuptools" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} setuptools"

	@pip install -U --no-cache-dir -q -r requirements.txt &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS"

.PHONY: prepare
prepare: header ## Install ansible-galaxy requirements
	@echo "***************************** ANSIBLE REQUIREMENTS *****************************"
	@ansible-galaxy install -fr ${PWD}/requirements.yml

##
## —————————————— TESTING —————————————————————————————————————————————————————————
##
.PHONY: tests-docker-local
tests-docker-local: header ## Testing docker role apply on localhost
	@echo "${Blue}Testing docker role apply on localhost${Color_Off}"
	@cd ${TEST_LOCALHOST_DOCKER_DIRECTORY} && ansible-playbook tests.yml

.PHONY: tests-docker-local-stop
tests-docker-local-stop: header ## Testing docker role apply on localhost
	@echo "${Blue}Testing docker role apply on localhost${Color_Off}"
	@cd ${TEST_LOCALHOST_DOCKER_DIRECTORY} && ansible-playbook tests.yml -e 'docker_action=stop'

##
## —————————————— CLEAN ———————————————————————————————————————————————————————————
##

.PHONY: clean
clean: ## Easy way to clean-up local environment
	@echo "${Green}Clean up environment${Color_Off}"
	@rm -rf .direnv
