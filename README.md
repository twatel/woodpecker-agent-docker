# ansible-role-woodpecker-agent

This role has goal to install and configure woodpecker agent (docker based)

It configure :  
* docker network
* docker container (woodpecker-agent)

## Setup environment 
```shell
sudo make install-python
sudo apt-get install direnv -y
if [ ! "$(grep -ir "direnv hook bash" ~/.bashrc)" ];then echo 'eval "$(direnv hook bash)"' >> ~/.bashrc; fi && direnv allow . && source ~/.bashrc
make env prepare
```

## Testing ansible-role-woodpecker-agent execution in vagrant-docker environment
```shell
make tests-docker-local
```

## Variables
Take a look at **roles/ansible-role-woodpecker-agent/defaults/main.yml** file to have more informations about variables  

## To know
This role has only one requirement : 
* You must have an woodpecker-server host ready

Don't panic, you can use this role to install and configure an woodpecker-server :
* https://codeberg.org/twatel/ansible-role-woodpecker-server
